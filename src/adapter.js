const axios = require('axios');
import { buildUri } from './utils'

const projectUUID = process.env['projectUUID']
export class DatabaseAPI {

    constructor() {}

    getLeaderBoard() {
        return new Promise((resolve, reject) => {
            axios.get(buildUri(`/api/ext/score/?reference_uuid=${projectUUID}`))
                .then(function (response) {
                    console.log('database-api success', response);
                    resolve(response.data)
                })
                .catch(function (error) {
                    console.log('database-api error', error);
                    reject(error)
                })
                .finally(function () {
                    // always executed
                });
        })
    }

    postScoreData(scoreObject) {
        scoreObject['reference_uuid'] = projectUUID;
        return new Promise((resolve, reject) => {
            axios.post(buildUri('/api/ext/score/'), scoreObject)
                .then(function (response) {
                    console.log('database-api success', response);
                    resolve(response)
                })
                .catch(function (error) {
                    console.log('database-api error', error);
                    reject(error)
                })
                .finally(function () {
                    // always executed
                });
        })
    }

}