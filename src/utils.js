const domain = process.env['dbDomain'];

export function buildUri(path) {
    return `${domain}${path}`
}